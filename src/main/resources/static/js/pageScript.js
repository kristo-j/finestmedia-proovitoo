$(document).ready(function() {
    var url = window.location.origin;
    var jsonData;
    var calendarStartDate = new Date();
    calendarStartDate.setFullYear( calendarStartDate.getFullYear() - 2 );
    var calendarEndDate = new Date();

    $('#startdate-input').datepicker({
        language: 'en',
        minDate: calendarStartDate,
        maxDate: calendarEndDate,
        dateFormat: 'yyyy-mm-dd'
    });
    $('#enddate-input').datepicker({
        language: 'en',
        minDate: calendarStartDate,
        maxDate: calendarEndDate,
        dateFormat: 'yyyy-mm-dd'
    });

    function initDatatable(data) {
        $('#consumption-table').DataTable({
            destroy: true,
            data: data,
            columns: [
                {data: 'consumptionDateString'},
                {data: 'consumptionAmount'},
                {data: 'consumptionPrice'}
            ]
        });
    }

    function isValidDate(dateString) {
        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        if(!dateString.match(regEx)) {
            return false;
        }
        var d = new Date(dateString);
        if(!d.getTime() && d.getTime() !== 0) {
            return false;
        }

        return true;
    }

    function controlInput(startdate, enddate) {
        var dateStart = new Date(startdate);
        var dateEnd = new Date(enddate);

        if(!isValidDate(startdate)) {
            $.notify('Algus kuupäev pole kuupäev.(YYYY-MM-DD)', 'warn');
            $('#startdate-input').addClass('error');
            return false;
        } else {
            $('#startdate-input').removeClass('error');
        }

        if(!isValidDate(enddate)) {
            $.notify('Lõpp kuupäev pole kuupäev.(YYYY-MM-DD)', 'warn');
            $('#enddate-input').addClass('error');
            return false;
        } else {
            $('#enddate-input').removeClass('error');
        }

        if(dateStart > calendarEndDate || dateStart < calendarStartDate) {
            $.notify('Kuupäeva vahemik alates tänasest kuni 2a tagasi.', 'warn');
            $('#startdate-input').addClass('error');
            return false;
        } else {
            $('#startdate-input').removeClass('error');
        }

        if(dateEnd > calendarEndDate || dateEnd < calendarStartDate) {
            $.notify('Kuupäeva vahemik alates tänasest kuni 2a tagasi.', 'warn');
            $('#enddate-input').addClass('error');
            return false;
        } else {
            $('#enddate-input').removeClass('error');
        }

/*
        if(dateStart instanceof Date) {
            $('#startdate-input').removeClass('error');
        } else {
            $.notify('Algus kuupäev pole kuupäev', 'warn');
            $('#startdate-input').addClass('error');
            return false;
        }

        if(dateEnd instanceof Date) {
            $('#endDate-input').removeClass('error');
        } else {
            $.notify('Lõpp kuupäev pole kuupäev', 'warn');
            $('#enddate-input').addClass('error');
            return false;
        }
*/
        if(dateEnd<dateStart) {
            $.notify('Algus kuupäev ei saa olle suurem kui lõpp kuupäev ja vastupidi!', 'warn');
            $('#startdate-input').addClass('error');
            $('#enddate-input').addClass('error');
            return false;
        } else {
            $('#startdate-input').removeClass('error');
            $('#enddate-input').removeClass('error');
        }

        return true;
    }

    $('.get-data').on('click', function() {
        var startdate = $('#startdate-input').val();
        var enddate = $('#enddate-input').val();
        var price = $('#price').val();
        var groupFilter;

        if(!controlInput(startdate, enddate, price)) {
            return;
        }

        if(!$('#price').val()) {
            $.notify('Hind peab olema määratud!' ,'warn');
            $('#price').addClass('error');
            return;
        } else {
            $('#price').removeClass('error');
        }

        if($('.selected').length) {
            groupFilter = $('.selected').attr("name");
        } else {
            groupFilter = "none";
        }

        $.ajax({
            method: 'get',
            url: url+'/api/electricity',
            data: {
                startdate: startdate.toString(),
                enddate: enddate.toString(),
                price: price,
                group: groupFilter
            },
            success: function(data) {
                initDatatable(data);
            }
        });

    });

    $('.group-button').on('click', function(){
        if($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            $('.group-button').each(function(){
                if($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
            });

            $(this).addClass('selected');
        }
    });

});