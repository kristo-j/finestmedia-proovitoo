package com.finestmeida.proov.electricity;

import org.decimal4j.util.DoubleRounder;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;

public class ElectricityConsumption {

    private ZonedDateTime consumptionDate;
    private double consumptionAmount;
    private double consumptionPrice;
    private String consumptionDateString;

    public ElectricityConsumption() {
    }

    public ElectricityConsumption(ZonedDateTime consumptionDate, double consumptionAmount) {
        this.consumptionDate = consumptionDate;
        this.consumptionAmount = consumptionAmount;
    }

    public ElectricityConsumption(ZonedDateTime consumptionDate, double consumptionAmount, double consumptionPrice) {
        this.consumptionDate = consumptionDate;
        this.consumptionAmount = consumptionAmount;
        this.calculateConsumptionCost(consumptionPrice);
        this.convertDateLocalToString("none");
    }

    public ZonedDateTime getConsumptionDate() {
        return consumptionDate;
    }

    public void setConsumptionDate(ZonedDateTime consumptionDate) {
        this.consumptionDate = consumptionDate;
    }

    public double getConsumptionAmount() {
        return consumptionAmount;
    }

    public void setConsumptionAmount(double consumptionAmount) {
        this.consumptionAmount = consumptionAmount;
    }

    public double getConsumptionPrice() {
        return consumptionPrice;
    }

    public void setConsumptionPrice(double consumptionPrice) {
        this.consumptionPrice = consumptionPrice;
    }

    public String getConsumptionDateString() {
        return consumptionDateString;
    }

    public void setConsumptionDateString(String consumptionDateString) {
        this.consumptionDateString = consumptionDateString;
    }

    public void addElectricityConsumption(double consumptionAmount) {
        this.consumptionAmount += consumptionAmount;
    }

    public void roundElectricityConsumption() {
        this.consumptionAmount = DoubleRounder.round(this.consumptionAmount, 3);
    }

    public void calculateConsumptionCost(double price) {
        Double calculatedPrice;
        calculatedPrice = this.consumptionAmount * price;
        this.consumptionPrice = DoubleRounder.round(calculatedPrice, 3);
    }

    public boolean checkIfDaysMatch(ZonedDateTime localDate) {
        if(this.consumptionDate.toLocalDate().equals(localDate.toLocalDate())) {
            return true;
        }

        return false;
    }

    public boolean checkIfMonthsMatch(ZonedDateTime localDate) {
        if(this.consumptionDate.getMonthValue() == localDate.getMonthValue()) {
            return true;
        }
        return false;
    }

    public boolean chechIfWeeksMatch(ZonedDateTime localDate) {
        Date dateLoc = java.sql.Date.valueOf(localDate.toLocalDate());
        Date dateThis = java.sql.Date.valueOf(this.consumptionDate.toLocalDate());

        int localWeekNr = Integer.parseInt(new SimpleDateFormat("w").format(dateLoc));
        int thisWeekNr = Integer.parseInt(new SimpleDateFormat("w").format(dateThis));

        if(localWeekNr == thisWeekNr) {
            return true;
        }

        return false;
    }

    public void convertDateLocalToString(String filterGroup) {
        switch(filterGroup) {
            case "day":
                this.consumptionDateString = this.consumptionDate.toLocalDate().toString();
                break;
            case "week":
                Date dateThis = java.sql.Date.valueOf(this.consumptionDate.toLocalDate());
                int thisWeekNr = Integer.parseInt(new SimpleDateFormat("w").format(dateThis));

                this.consumptionDateString = String.format("%d - Nädal %s", this.consumptionDate.getYear(),
                                                                          thisWeekNr);

                this.consumptionDate.toLocalDateTime().toString();
                break;
            case "month":
                this.consumptionDateString = String.format("%d-%d", this.consumptionDate.getYear(),
                                                                    this.consumptionDate.getMonthValue());
                break;
            case "none":
                this.consumptionDateString = this.consumptionDate.toLocalDateTime().toString();
                break;
        }
    }


    public String toString() {
        return String.format("Electricity consumption {date: %s; amount: %d; price: %d;}",
                this.getConsumptionDateString(), this.getConsumptionAmount(), this.getConsumptionPrice());
    }

}
