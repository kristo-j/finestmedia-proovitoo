package com.finestmeida.proov.electricity;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;

@RestController
@EnableCaching
public class ElectricityConsumptionController {

    private Document loadTestDocument(String url) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return factory.newDocumentBuilder().parse(new URL(url).openStream());
    }

    @Cacheable("ElectricityConsumption")
    @RequestMapping("/api/electricity")
    public ArrayList getElectricityConsumptionList (@RequestParam("startdate") String startdate,
                            @RequestParam("enddate") String enddate,
                            @RequestParam("price") Double price,
                            @RequestParam("group") String group) throws Exception {

        Document doc = loadTestDocument("https://finestmedia.ee/kwh/?start="+startdate+"&end="+enddate);
        NodeList docNodes = doc.getElementsByTagName("HourConsumption");
        ArrayList entities = new ArrayList<ElectricityConsumption>();
        ElectricityConsumption electricityConsumptionEntity = null;
        Node node;

        boolean allowInsert = false;

        LocalDate startDate = LocalDate.parse(startdate);
        LocalDate endDate = LocalDate.parse(enddate);
        ZonedDateTime zdt;

        for(int i=0; i<docNodes.getLength(); i++) {
            node = docNodes.item(i);

            zdt = ZonedDateTime.parse(node.getAttributes().getNamedItem("ts").getNodeValue());

            if(startDate.isAfter(zdt.toLocalDate()) || endDate.isBefore(zdt.toLocalDate())) {
                continue;
            }

            switch(group) {
                case "day":
                    if(electricityConsumptionEntity == null) {
                        electricityConsumptionEntity = new ElectricityConsumption(zdt,
                                                                                  Double.parseDouble(node.getTextContent()));
                    }

                    if(electricityConsumptionEntity.checkIfDaysMatch(zdt)) {
                        electricityConsumptionEntity.addElectricityConsumption(Double.parseDouble(node.getTextContent()));
                        allowInsert = true;
                    } else {
                        electricityConsumptionEntity.calculateConsumptionCost(price);
                        electricityConsumptionEntity.convertDateLocalToString(group);
                        entities.add(electricityConsumptionEntity);

                        electricityConsumptionEntity = new ElectricityConsumption(zdt,
                                                            Double.parseDouble(node.getTextContent()));
                        allowInsert = false;
                    }
                    break;
                case "week":
                    if(electricityConsumptionEntity == null) {
                        electricityConsumptionEntity = new ElectricityConsumption(zdt,
                                Double.parseDouble(node.getTextContent()));
                    }

                    if(electricityConsumptionEntity.chechIfWeeksMatch(zdt)) {
                        electricityConsumptionEntity.addElectricityConsumption(Double.parseDouble(node.getTextContent()));
                        allowInsert = true;
                    } else {
                        electricityConsumptionEntity.convertDateLocalToString(group);
                        electricityConsumptionEntity.calculateConsumptionCost(price);
                        entities.add(electricityConsumptionEntity);

                        electricityConsumptionEntity = new ElectricityConsumption(zdt,
                                Double.parseDouble(node.getTextContent()));
                        allowInsert = false;
                    }
                    break;
                case "month":
                    if(electricityConsumptionEntity == null) {
                        electricityConsumptionEntity = new ElectricityConsumption(zdt,
                                                            Double.parseDouble(node.getTextContent()));
                    }

                    if(electricityConsumptionEntity.checkIfMonthsMatch(zdt)) {
                        electricityConsumptionEntity.addElectricityConsumption(Double.parseDouble(node.getTextContent()));
                        allowInsert = true;
                    } else {
                        electricityConsumptionEntity.convertDateLocalToString(group);
                        electricityConsumptionEntity.calculateConsumptionCost(price);
                        entities.add(electricityConsumptionEntity);

                        electricityConsumptionEntity = new ElectricityConsumption(zdt,
                                Double.parseDouble(node.getTextContent()));
                        allowInsert = false;
                    }
                    break;
                default:
                    entities.add(new ElectricityConsumption(zdt,
                                                            Double.parseDouble(node.getTextContent()),
                                                            price));
                    break;

            }
        }

        if(allowInsert) {
            electricityConsumptionEntity.convertDateLocalToString(group);
            electricityConsumptionEntity.calculateConsumptionCost(price);
            entities.add(electricityConsumptionEntity);
        }

        return entities;
    }
}
